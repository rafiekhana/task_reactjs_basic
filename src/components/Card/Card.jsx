import React from 'react';
import styleCard from './Card.module.css';

const Card = ({ name, address }) => {
  return (
    <div className={styleCard["card-style"]}>
      <h1>Name: {name}</h1>
      <h1>Address: {address}</h1>
    </div>
  )
}

export default Card;