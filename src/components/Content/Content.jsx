import React from 'react';
import Card from "../Card/Card";
import styleContent from './Content.module.css';

class Content extends React.Component {
  constructor() {
    super()
    this.state = {
      name: "Rafie Kotjek",
      address: "Indonesia",
    };
  };

  render() {
    return (
      <div className={styleContent["content-style"]}> 
        <Card name={this.state.name} address={this.state.address} />
        <button className={styleContent["btn-style"]} onClick={() => this.setState({name: "Rafie"})}>
          Change Name
        </button>
        <button className={styleContent["btn-style"]} onClick={() => this.setState({ address: "Jakarta"})}>
          Change Address
        </button>
      </div>
    )
  }
};

export default Content